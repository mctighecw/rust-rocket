# README

A basic [Rust language](https://www.rust-lang.org) web server using the [Rocket](https://rocket.rs) framework.

## App Information

App Name: rust-rocket

Created: February 2024

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/rust-rocket)

## Tech Stack

- Rust (1.76.0)
- Rocket (0.5.0)
- Docker

## To Run

```sh
$ docker build . \
  -f Dockerfile.dev \
  -t rust-rocket && \
  docker run \
  --rm \
  -it \
  --name rust-rocket \
  -p 8000:8000 rust-rocket
```

Last updated: 2024-11-12
